package org.earandap.snoop.test.controller;

import org.earandap.snoop.test.dao.WorkspaceDAO;
import org.earandap.snoop.test.domain.Workspace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by eduardo on 4/2/2015.
 */
@Controller
public class RestController {

    @Autowired
    private WorkspaceDAO workspaceDAO;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ResponseBody
    public  Workspace get (@PathVariable long id){
        Workspace workspace = workspaceDAO.obtener(id);

        return workspace;
    }
}
