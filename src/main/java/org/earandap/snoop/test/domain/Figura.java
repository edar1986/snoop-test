package org.earandap.snoop.test.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by eduardo on 4/1/2015.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.MINIMAL_CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Cuadrado.class),
        @JsonSubTypes.Type(value = Hexagono.class),
        @JsonSubTypes.Type(value = Triangulo.class),
})
public abstract class Figura {

    private int numLados;

    public Figura(int numLados) {
        this.numLados = numLados;
    }

    public abstract double getArea();

    public abstract double getPerimetro();

    public int getNumLados() {
        return numLados;
    }

    public void setNumLados(int numLados) {
        this.numLados = numLados;
    }

}
