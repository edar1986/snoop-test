package org.earandap.snoop.test.domain;

/**
 * Created by eduardo on 4/1/2015.
 */
public class Cuadrado extends Figura {

    private int lado;

    public Cuadrado(){
        this(0);
    }

    public Cuadrado(int lado) {
        super(4);
        this.lado = lado;
    }

    @Override
    public double getArea() {
        return Math.pow(lado,2);
    }

    @Override
    public double getPerimetro() {
        return 4 * lado;
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }
}
