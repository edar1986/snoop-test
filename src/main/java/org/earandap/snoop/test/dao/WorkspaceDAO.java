package org.earandap.snoop.test.dao;

import org.earandap.snoop.test.domain.Figura;
import org.earandap.snoop.test.domain.Workspace;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by eduardo on 4/1/2015.
 */

public interface WorkspaceDAO {

    public long guardar(Workspace workspace);

    public Workspace obtener(long id);

    public void eliminar(long id);

    public Collection<Workspace> listar();

}
