package org.earandap.snoop.test.dao.impl;

import org.earandap.snoop.test.dao.WorkspaceDAO;
import org.earandap.snoop.test.domain.Cuadrado;
import org.earandap.snoop.test.domain.Figura;
import org.earandap.snoop.test.domain.Hexagono;
import org.earandap.snoop.test.domain.Workspace;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by eduardo on 4/1/2015.
 */
@Repository
public class WorkspaceInMemoryDAOImpl implements WorkspaceDAO {

    private Map<Long, Workspace> datos = new HashMap<>();
    private long id = 1;


    public WorkspaceInMemoryDAOImpl() {
        Workspace workspace = new Workspace("Con Hexágono", 2);
        Cuadrado c = new Cuadrado(4);
        workspace.agregarFigura(c);
        Hexagono hexagono = new Hexagono(3);
        workspace.agregarFigura(hexagono);
        guardar(workspace);
        workspace = new Workspace("Sin Hexágono", 2);
        c = new Cuadrado(3);
        workspace.agregarFigura(c);

        guardar(workspace);
    }

    @Override
    public long guardar(Workspace workspace) {
        if (workspace.getId() == 0) {
            workspace.setId(id++);
        }
        datos.put(workspace.getId(), workspace);

        return workspace.getId();
    }

    @Override
    public Workspace obtener(long id) {
        return datos.get(id);
    }

    @Override
    public void eliminar(long id) {
        datos.remove(id);
    }

    @Override
    public Collection<Workspace> listar() {
        return datos.values();
    }






}
